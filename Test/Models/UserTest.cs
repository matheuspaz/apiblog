﻿using System;
using Xunit;
using APIBlog.Models;

namespace Test.Models
{
    public class UserTest
    {
        [Fact]
        public void TestGenerateRandomToken()
        {
            User user = new User();
            user.GenerateToken();

            Assert.NotEmpty(user.Token);
        }

        [Fact]
        public void TestGenerateRandomPassword()
        {
            User user = new User();
            user.GeneratePassword("password");

            Assert.NotEmpty(user.Password);
            Assert.NotEmpty(user.Salt);
        }

        [Fact]
        public void TestUsernamePropertyToLower()
        {
            User user = new User();
            user.Username = "TestUsername";

            string expectedResult = "TestUsername".ToLower();
            Assert.Equal(expectedResult, user.Username);
        }

        [Fact]
        public void TestCheckPassword()
        {
            User user = new User();
            user.GeneratePassword("password");

            //Correct password
            bool passwordCorrectResult = user.PasswordCheck("password");
            Assert.True(passwordCorrectResult);

            //Wrong password
            bool passwordWrongResult = user.PasswordCheck("wrongpassword");
            Assert.False(passwordWrongResult);
        }
    }
}
