﻿using System;
using System.Collections.Generic;
using System.Linq;
using APIBlog.Data;
using APIBlog.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;

namespace APIBlog.Controllers
{
    public class UserController : Controller
    {
        private DatabaseContext databaseContext;
        private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        private int randomPasswordLenght = 10;

        public UserController()
        {
             databaseContext = new DatabaseContext();
        }

        // Get all user list
        [HttpGet("users")]
        public IActionResult Get()
        {
            IList<User> users = databaseContext.Users.ToList();

            if(users.Count == 0)
            {
                return StatusCode(StatusCodes.Status204NoContent);
            }

            return StatusCode(StatusCodes.Status200OK, users);
        }

        // Get user from database
        [HttpGet("user/{id}")]
        public IActionResult Get(int id)
        {
            User user = databaseContext.Users.Where(u => u.Id == id).FirstOrDefault();

            if(user == null)
            {
                return StatusCode(StatusCodes.Status204NoContent);
            }

            return StatusCode(StatusCodes.Status200OK, user);
        }

        // Insert user in database
        [HttpPost("user")]
        public IActionResult Post([FromBody]User user)
        {
            user.Username = user.Username.Replace(" ", "");
            User databaseUser = databaseContext.Users.Where(u => u.Username == user.Username).FirstOrDefault();

            if (databaseUser != null || String.IsNullOrEmpty(user.Username) || String.IsNullOrWhiteSpace(user.Username))
            {
                return StatusCode(StatusCodes.Status406NotAcceptable);
            }

            Random random = new Random();
            string randomPassword = new String(Enumerable.Repeat(chars, randomPasswordLenght)
                .Select(s => s[random.Next(s.Length)]).ToArray());


            user.GeneratePassword(randomPassword);
            user.GenerateToken();
            user.DefaultPassword = true;

            databaseContext.Users.Add(user);
            databaseContext.SaveChanges();

            return StatusCode(StatusCodes.Status201Created, JObject.Parse("{'password': '" + randomPassword + "'}"));
        }

        // Delete user from database
        [HttpDelete("user/{id}")]
        public IActionResult Delete(int id)
        {
            User user = databaseContext.Users.Where(u => u.Id == id).FirstOrDefault();

            if(user == null)
            {
                return StatusCode(StatusCodes.Status204NoContent);
            }

            databaseContext.Users.Remove(user);
            databaseContext.SaveChanges();

            return StatusCode(StatusCodes.Status200OK);
        }
    }
}
