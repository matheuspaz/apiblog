﻿using APIBlog.Interfaces;
using APIBlog.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace APIBlog.Data
{
    public class DatabaseContext : DbContext
    {
        // Connection Properties
        public string DbUsername { get; }
        public string DbPassword { get; }
        public string DbName { get; }
        public string DbHost { get; }

        // Set the database tables
        public DbSet<User> Users { get; set; }

        public DatabaseContext()
        {
            // Set the connection parameters 
            DbUsername = "Blog";
            DbPassword = "Blog";
            DbName = "Blog";
            DbHost = "(localdb)\\MSSQLLocalDB";
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer($"Data Source = {DbHost}; Database = {DbName}; User ID = {DbUsername}; Password = {DbPassword}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Populate admin user data
            User adminUser = new User()
            {
                Id = 1,
                Username = "Admin",
                DefaultPassword = true,
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };
            adminUser.GeneratePassword("Admin@2019");

            modelBuilder.Entity<User>().HasData(adminUser);
        }

        public override int SaveChanges()
        {
            // Do a created at and modified at automatic
            var newEntities = this.ChangeTracker.Entries()
                .Where(
                    x => x.State == EntityState.Added &&
                    x.Entity != null &&
                    x.Entity as ITimeStampedModel != null
                    )
                .Select(x => x.Entity as ITimeStampedModel);

            var modifiedEntities = this.ChangeTracker.Entries()
                .Where(
                    x => x.State == EntityState.Modified &&
                    x.Entity != null &&
                    x.Entity as ITimeStampedModel != null
                    )
                .Select(x => x.Entity as ITimeStampedModel);

            foreach (var newEntity in newEntities)
            {
                newEntity.CreatedAt = DateTime.Now;
                newEntity.ModifiedAt = DateTime.Now;
            }

            foreach (var modifiedEntity in modifiedEntities)
            {
                modifiedEntity.ModifiedAt = DateTime.Now;
            }

            return base.SaveChanges();
        }
    }
}
