﻿using APIBlog.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;

namespace APIBlog.Models
{
    public class User : ITimeStampedModel
    {
        private string _username;
        private HMACSHA256 hmac;

        public int Id { get; set; }
        [Required]
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value.ToLower();
            }
        }
        [Required]
        public string Password { get; set; }
        public string Token { get; private set; }
        public string Salt { get; private set; }
        public bool DefaultPassword { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }

        public User()
        {
            hmac = new HMACSHA256(); 
        }

        public void GenerateToken()
        {
            Token = Guid.NewGuid().ToString("N") + Guid.NewGuid().ToString("N") + DateTime.Now.Millisecond;
        }

        public void GeneratePassword(string password)
        {
            Salt = Guid.NewGuid().ToString("N") + Guid.NewGuid().ToString("N") + DateTime.Now.Millisecond;
            hmac.Key = Encoding.UTF8.GetBytes(Salt);
            byte[] passwordByte = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            Password = Convert.ToBase64String(passwordByte);
        }

        public bool PasswordCheck(string password)
        {
            hmac.Key = Encoding.UTF8.GetBytes(Salt);
            byte[] passwordByte = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            password = Convert.ToBase64String(passwordByte);
            if (Password.Equals(password))
            {
                return true;
            }

            return false;
        }
    }
}
