﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace APIBlog.Migrations
{
    public partial class CreateUsersTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Username = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false),
                    Token = table.Column<string>(nullable: true),
                    Salt = table.Column<string>(nullable: true),
                    DefaultPassword = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedAt", "DefaultPassword", "ModifiedAt", "Password", "Salt", "Token", "Username" },
                values: new object[] { 1, new DateTime(2019, 2, 11, 11, 9, 44, 717, DateTimeKind.Local), true, new DateTime(2019, 2, 11, 11, 9, 44, 721, DateTimeKind.Local), "fzEdfOlb8I1MmJPiSqd0ke0Rs2w2kpMo8cBAFyVHfZM=", "52adb325728b49809ba3314911c6e6452995af82c7214ff0b3de897d276429ef", "fb72c3246fd342288eac2a98035c518bea2e1bf702e24c6da8c78d8d35922c96", "admin" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
