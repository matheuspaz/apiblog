﻿// <auto-generated />
using System;
using APIBlog.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace APIBlog.Migrations
{
    [DbContext(typeof(DatabaseContext))]
    [Migration("20190211130945_CreateUsersTable")]
    partial class CreateUsersTable
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("APIBlog.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<bool>("DefaultPassword");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("Password")
                        .IsRequired();

                    b.Property<string>("Salt");

                    b.Property<string>("Token");

                    b.Property<string>("Username")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Users");

                    b.HasData(
                        new { Id = 1, CreatedAt = new DateTime(2019, 2, 11, 11, 9, 44, 717, DateTimeKind.Local), DefaultPassword = true, ModifiedAt = new DateTime(2019, 2, 11, 11, 9, 44, 721, DateTimeKind.Local), Password = "fzEdfOlb8I1MmJPiSqd0ke0Rs2w2kpMo8cBAFyVHfZM=", Salt = "52adb325728b49809ba3314911c6e6452995af82c7214ff0b3de897d276429ef", Token = "fb72c3246fd342288eac2a98035c518bea2e1bf702e24c6da8c78d8d35922c96", Username = "admin" }
                    );
                });
#pragma warning restore 612, 618
        }
    }
}
